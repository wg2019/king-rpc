// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// UserCenterClient is the client API for UserCenter service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UserCenterClient interface {
	// 注册用户
	Register(ctx context.Context, in *RegisterParam, opts ...grpc.CallOption) (*User, error)
	// 注销用户
	LogOff(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*Bool, error)
	// 绑定来源
	BindSource(ctx context.Context, in *BindSourceParam, opts ...grpc.CallOption) (*Bool, error)
	// 解绑来源
	UnBindSource(ctx context.Context, in *BindSourceParam, opts ...grpc.CallOption) (*Bool, error)
	// 获取用户信息
	UserInfo(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*User, error)
	// 获取来源列表
	UserSourceList(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*SourceList, error)
	// 根据用户ID列表批量获取用户列表
	UserList(ctx context.Context, in *UserIdListParam, opts ...grpc.CallOption) (*UserInfoList, error)
	// 获取用户相关信息
	UserRelated(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*Related, error)
}

type userCenterClient struct {
	cc grpc.ClientConnInterface
}

func NewUserCenterClient(cc grpc.ClientConnInterface) UserCenterClient {
	return &userCenterClient{cc}
}

func (c *userCenterClient) Register(ctx context.Context, in *RegisterParam, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/api.UserCenter/Register", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) LogOff(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*Bool, error) {
	out := new(Bool)
	err := c.cc.Invoke(ctx, "/api.UserCenter/LogOff", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) BindSource(ctx context.Context, in *BindSourceParam, opts ...grpc.CallOption) (*Bool, error) {
	out := new(Bool)
	err := c.cc.Invoke(ctx, "/api.UserCenter/BindSource", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) UnBindSource(ctx context.Context, in *BindSourceParam, opts ...grpc.CallOption) (*Bool, error) {
	out := new(Bool)
	err := c.cc.Invoke(ctx, "/api.UserCenter/UnBindSource", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) UserInfo(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/api.UserCenter/UserInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) UserSourceList(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*SourceList, error) {
	out := new(SourceList)
	err := c.cc.Invoke(ctx, "/api.UserCenter/UserSourceList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) UserList(ctx context.Context, in *UserIdListParam, opts ...grpc.CallOption) (*UserInfoList, error) {
	out := new(UserInfoList)
	err := c.cc.Invoke(ctx, "/api.UserCenter/UserList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userCenterClient) UserRelated(ctx context.Context, in *UserIdParam, opts ...grpc.CallOption) (*Related, error) {
	out := new(Related)
	err := c.cc.Invoke(ctx, "/api.UserCenter/UserRelated", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserCenterServer is the server API for UserCenter service.
// All implementations must embed UnimplementedUserCenterServer
// for forward compatibility
type UserCenterServer interface {
	// 注册用户
	Register(context.Context, *RegisterParam) (*User, error)
	// 注销用户
	LogOff(context.Context, *UserIdParam) (*Bool, error)
	// 绑定来源
	BindSource(context.Context, *BindSourceParam) (*Bool, error)
	// 解绑来源
	UnBindSource(context.Context, *BindSourceParam) (*Bool, error)
	// 获取用户信息
	UserInfo(context.Context, *UserIdParam) (*User, error)
	// 获取来源列表
	UserSourceList(context.Context, *UserIdParam) (*SourceList, error)
	// 根据用户ID列表批量获取用户列表
	UserList(context.Context, *UserIdListParam) (*UserInfoList, error)
	// 获取用户相关信息
	UserRelated(context.Context, *UserIdParam) (*Related, error)
	mustEmbedUnimplementedUserCenterServer()
}

// UnimplementedUserCenterServer must be embedded to have forward compatible implementations.
type UnimplementedUserCenterServer struct {
}

func (UnimplementedUserCenterServer) Register(context.Context, *RegisterParam) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Register not implemented")
}
func (UnimplementedUserCenterServer) LogOff(context.Context, *UserIdParam) (*Bool, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LogOff not implemented")
}
func (UnimplementedUserCenterServer) BindSource(context.Context, *BindSourceParam) (*Bool, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BindSource not implemented")
}
func (UnimplementedUserCenterServer) UnBindSource(context.Context, *BindSourceParam) (*Bool, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UnBindSource not implemented")
}
func (UnimplementedUserCenterServer) UserInfo(context.Context, *UserIdParam) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserInfo not implemented")
}
func (UnimplementedUserCenterServer) UserSourceList(context.Context, *UserIdParam) (*SourceList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserSourceList not implemented")
}
func (UnimplementedUserCenterServer) UserList(context.Context, *UserIdListParam) (*UserInfoList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserList not implemented")
}
func (UnimplementedUserCenterServer) UserRelated(context.Context, *UserIdParam) (*Related, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserRelated not implemented")
}
func (UnimplementedUserCenterServer) mustEmbedUnimplementedUserCenterServer() {}

// UnsafeUserCenterServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UserCenterServer will
// result in compilation errors.
type UnsafeUserCenterServer interface {
	mustEmbedUnimplementedUserCenterServer()
}

func RegisterUserCenterServer(s grpc.ServiceRegistrar, srv UserCenterServer) {
	s.RegisterService(&UserCenter_ServiceDesc, srv)
}

func _UserCenter_Register_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RegisterParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).Register(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/Register",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).Register(ctx, req.(*RegisterParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_LogOff_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIdParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).LogOff(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/LogOff",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).LogOff(ctx, req.(*UserIdParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_BindSource_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BindSourceParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).BindSource(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/BindSource",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).BindSource(ctx, req.(*BindSourceParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_UnBindSource_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BindSourceParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).UnBindSource(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/UnBindSource",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).UnBindSource(ctx, req.(*BindSourceParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_UserInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIdParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).UserInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/UserInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).UserInfo(ctx, req.(*UserIdParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_UserSourceList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIdParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).UserSourceList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/UserSourceList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).UserSourceList(ctx, req.(*UserIdParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_UserList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIdListParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).UserList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/UserList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).UserList(ctx, req.(*UserIdListParam))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserCenter_UserRelated_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIdParam)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserCenterServer).UserRelated(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.UserCenter/UserRelated",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserCenterServer).UserRelated(ctx, req.(*UserIdParam))
	}
	return interceptor(ctx, in, info, handler)
}

// UserCenter_ServiceDesc is the grpc.ServiceDesc for UserCenter service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UserCenter_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.UserCenter",
	HandlerType: (*UserCenterServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Register",
			Handler:    _UserCenter_Register_Handler,
		},
		{
			MethodName: "LogOff",
			Handler:    _UserCenter_LogOff_Handler,
		},
		{
			MethodName: "BindSource",
			Handler:    _UserCenter_BindSource_Handler,
		},
		{
			MethodName: "UnBindSource",
			Handler:    _UserCenter_UnBindSource_Handler,
		},
		{
			MethodName: "UserInfo",
			Handler:    _UserCenter_UserInfo_Handler,
		},
		{
			MethodName: "UserSourceList",
			Handler:    _UserCenter_UserSourceList_Handler,
		},
		{
			MethodName: "UserList",
			Handler:    _UserCenter_UserList_Handler,
		},
		{
			MethodName: "UserRelated",
			Handler:    _UserCenter_UserRelated_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/user_center.proto",
}
