.PHONY: pb

# 新的protoc-gen-go插件已经不支持plugins选项
pb:
	protoc --proto_path= \
	--go_out=. \
	--go-grpc_out=. \
	./proto/*.proto
	@echo ">>> make finish! "